# German translation tuxpaint-config.
# Copyright (C) 2014 the tuxpaint team.
# This file is distributed under the same license as the tuxpaint-config package.
# Andrej Žnidaršič <andrej.znidarsic@gmail.com>, 2009
# Matej Urbančič <mateju@svn.gnome.org>, 2009-2014.
#
msgid ""
msgstr ""
"Project-Id-Version: tuxpaint-config\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-14 00:38+0200\n"
"PO-Revision-Date: 2014-08-05 20:51+0100\n"
"Last-Translator: Matej Urbančič <mateju@svn.gnome.org>\n"
"Language-Team: Slovenian GNOME Translation Team <gnome-si@googlegroups.com>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : "
"n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: Poedit 1.5.4\n"

#: ../about.h:10
msgid ""
"Welcome to Tux Paint's graphical configuration tool, created by Torsten "
"Giebl, Jan Wynholds, Bill Kendrick, and Martin Fuhrer.\n"
"\n"
"This tool allows you to alter Tux Paint's settings, such as full-screen "
"mode, the interface's language, or options to simplify Tux Paint for younger "
"children.\n"
"\n"
"These settings can be set for the current user only or for all users of your "
"computer by making a selection in the 'Settings for' pull-down menu at the "
"bottom.\n"
"\n"
"Use the tabs at the top to select the different setting categories, change "
"the options you'd like to change, and click the 'Apply' button at the bottom "
"to write out a new configuration file.\n"
"\n"
"The next time Tux Paint is launched, the new settings should take effect."
msgstr ""
"Dobrodošli v grafično nastavitveno orodje Tux Slikarja, ki so ga ustvarili "
"Torsten Giebl, Jan Wynholds, Bill Kendrick in Martin Fuhrer.\n"
"\n"
"To orodje omogoča spremembo nastavitev Tux slikarja, kot so celozaslonski "
"način, jezik vmesnika, ali možnosti za poenostavitev Tux slikarja za manjše "
"otroke.\n"
"\n"
"Nastavitve je mogoče nastaviti za trenutnega uporabnika ali za vse "
"uporabnike z izbiro v meniju spustnega polja 'Nastavitve za' na dnu.\n"
"\n"
"Uporabite zavihke na vrhu za izbiro različnih kategorij nastavitev, "
"spremenite želene možnosti in kliknite gumb 'Uporabi' za zapis nove "
"nastavitvene datoteke.\n"
"\n"
"Spremembe bodo imele učinek ob ponovnemu zagonu Tux slikarja."

#: ../tuxpaint-config.cxx:208
msgid "(Use system's default)"
msgstr "(Uporabi privzeto sistemsko)"

#: ../tuxpaint-config2.cxx:92
msgid "(Use system's setting)"
msgstr "(Uporabi sistemsko nastavitev)"

#: ../tuxpaint-config2.cxx:1661 ../tuxpaint-config2.cxx:2342
#, c-format
msgid "Default (no override)"
msgstr "Privzeto (brez prepisa)"

#: ../tuxpaint-config2.cxx:1663
#, fuzzy, c-format
#| msgid "Override: Small"
msgid "Override: Small (%d)"
msgstr "Prepiši: Majhno"

#: ../tuxpaint-config2.cxx:1665
#, fuzzy, c-format
#| msgid "Override: Medium"
msgid "Override: Medium (%d)"
msgstr "Prepiši: Srednje"

#: ../tuxpaint-config2.cxx:1667
#, fuzzy, c-format
#| msgid "Override: Large"
msgid "Override: Large (%d)"
msgstr "Prepiši: Veliko"

#: ../tuxpaint-config2.cxx:1684 ../tuxpaint-config2.cxx:2165
#, fuzzy, c-format
#| msgid "Default (no override)"
msgid "Button size: 48x48 (default; no override)"
msgstr "Privzeto (brez prepisa)"

#: ../tuxpaint-config2.cxx:1686
#, c-format
msgid "Button size: %dx%d"
msgstr ""

#: ../tuxpaint-config2.cxx:1703 ../tuxpaint-config2.cxx:2178
#, fuzzy, c-format
#| msgid "Default (no override)"
msgid "Color palette rows: 1 (default; no override)"
msgstr "Privzeto (brez prepisa)"

#: ../tuxpaint-config2.cxx:1705
#, fuzzy, c-format
#| msgid "Color Palette File:"
msgid "Color palette rows: %d"
msgstr "Datoteka palete barv:"

#: ../tuxpaint-config2.cxx:1826
msgid "Save Directory?"
msgstr "Shranjevalna mapa?"

#: ../tuxpaint-config2.cxx:1843
#, fuzzy
#| msgid "Data Directory?"
msgid "Export Directory?"
msgstr "Podatkovna mapa?"

#: ../tuxpaint-config2.cxx:1875
msgid "Data Directory?"
msgstr "Podatkovna mapa?"

#: ../tuxpaint-config2.cxx:1906
msgid "Color Palette File?"
msgstr "Datoteka palete barv?"

#: ../tuxpaint-config2.cxx:1945
msgid "Quit without applying changes?"
msgstr "Končaj brez uporabe sprememb?"

#: ../tuxpaint-config2.cxx:1946
msgid "&No"
msgstr "&Ne"

#: ../tuxpaint-config2.cxx:1946
msgid "&Yes"
msgstr "&Da"

#: ../tuxpaint-config2.cxx:1982
msgid "current user"
msgstr "trenutni uporabnik"

#: ../tuxpaint-config2.cxx:1985
msgid "all users"
msgstr "vsi uporabniki"

#: ../tuxpaint-config2.cxx:1989
#, c-format
msgid "Apply changes you made to %s?"
msgstr "Ali naj se uporabijo spremembe narejene v %s?"

#: ../tuxpaint-config2.cxx:1990
msgid "No"
msgstr "Ne"

#: ../tuxpaint-config2.cxx:1990
msgid "Yes"
msgstr "Da"

#: ../tuxpaint-config2.cxx:2010 ../tuxpaint-config2.cxx:2013
msgid "Tux Paint Config v"
msgstr "Tux Paint nastavitve"

#. TAB: ABOUT
#: ../tuxpaint-config2.cxx:2022
msgid "About"
msgstr "O Programu"

#: ../tuxpaint-config2.cxx:2029
msgid "About Tux Paint Config."
msgstr "O nastavitvah Tux slikarja."

#. TAB: VIDEO / SOUND
#: ../tuxpaint-config2.cxx:2045
msgid "Video/Sound"
msgstr "Zvok in video"

#: ../tuxpaint-config2.cxx:2052
msgid "Video:"
msgstr "Video:"

#: ../tuxpaint-config2.cxx:2056
msgid "&Fullscreen"
msgstr "&Celozaslonski način"

#: ../tuxpaint-config2.cxx:2061
msgid "Run Tux Paint in fullscreen mode, rather than in a window."
msgstr "Zagon Tux slikarja v celozaslonskem načinu, namesto v oknu."

#: ../tuxpaint-config2.cxx:2067
msgid "&Native"
msgstr "&Lastno"

#: ../tuxpaint-config2.cxx:2071
msgid "Use native screen resolution in fullscreen mode."
msgstr "V celozaslonskem način uporabi lastno ločljivost zaslona "

#: ../tuxpaint-config2.cxx:2077
msgid "Window size : "
msgstr "Velikost okna:"

#: ../tuxpaint-config2.cxx:2101
msgid "Size of the window, or the resolution in fullscreen."
msgstr "Velikost okna ali ločljivost v celozaslonskem načinu. "

#: ../tuxpaint-config2.cxx:2107
msgid "&Rotate Orientation"
msgstr "&Zavrti usmeritev"

#: ../tuxpaint-config2.cxx:2111
msgid ""
"Swap width and height, to rotate 90 degrees (useful for portrait-display on "
"a tablet PC)"
msgstr ""
"Zamenjava širine in višine zavrti okno za 90 stopinj (uporabno za ležeč "
"prikaz na tabličnih računalnikih)"

#: ../tuxpaint-config2.cxx:2117
msgid "Allow S&creensaver"
msgstr "Dovoli o_hranjevalnik zaslona"

#: ../tuxpaint-config2.cxx:2121
msgid "Don't disable your system's screensaver while Tux Paint is running."
msgstr ""
"Ne onemogoči sistemskega ohranjevalnika zaslona medtem ko Tux slikar teče."

#: ../tuxpaint-config2.cxx:2130
msgid "Sound:"
msgstr "Zvok:"

#: ../tuxpaint-config2.cxx:2134
msgid "Enable &Sound Effects"
msgstr "Omogoči &zvočne učinke"

#: ../tuxpaint-config2.cxx:2139
msgid "Enable/disable sound effects."
msgstr "Omogoči/Onemogoči zvočne učinke."

#: ../tuxpaint-config2.cxx:2144
#, fuzzy
#| msgid "Enable &Sound Effects"
msgid "Enable S&tereo Sound"
msgstr "Omogoči &zvočne učinke"

#: ../tuxpaint-config2.cxx:2149
msgid "Stereophonic ('stereo') or monaural ('mono') sound."
msgstr ""

#: ../tuxpaint-config2.cxx:2157
#, fuzzy
#| msgid "Interface Simplification:"
msgid "Interface Size:"
msgstr "Poenostavitev vmesnika:"

#: ../tuxpaint-config2.cxx:2169
msgid ""
"(If button size is too large for the chosen window/screen resolution, the "
"largest usable size will be used.)"
msgstr ""

#. TAB: MOUSE / KEYBOARD
#: ../tuxpaint-config2.cxx:2188
msgid "Mouse/Keyboard"
msgstr "Miška in tipkovnica"

#: ../tuxpaint-config2.cxx:2196
msgid "Cursor:"
msgstr "Kazalec:"

#: ../tuxpaint-config2.cxx:2200
msgid "&Fancy Cursor Shapes"
msgstr "&Umetelne oblike kazalca"

#: ../tuxpaint-config2.cxx:2205
msgid ""
"Change the shape of the mouse pointer depending on where the mouse is and "
"what you are doing."
msgstr ""
"Spremeni obliko miškinega kazalnika glede na položaj miške in kaj počenjate."

#: ../tuxpaint-config2.cxx:2211
msgid "&Hide Cursor"
msgstr "&Skrij kazalec"

#: ../tuxpaint-config2.cxx:2216
msgid "Completely hides cursor (useful on touchscreen devices)."
msgstr "Popolnoma skrije kazalec (uporabno na napravah z zaslonom na dotik)."

#. Fl_Group* o
#: ../tuxpaint-config2.cxx:2225 ../tuxpaint-config2.cxx:2902
msgid "Keyboard:"
msgstr "Tipkovnica:"

#: ../tuxpaint-config2.cxx:2229
msgid "Enable &Keyboard Shortcuts"
msgstr "Omogoči &tipkovne bližnjice"

#: ../tuxpaint-config2.cxx:2234
msgid ""
"Allows key combinations to be used as shortcuts for certain commands. (e.g., "
"Control+S to Save)"
msgstr ""
"Dovoli uporabo kombinacij tipk kot bližnjice za določene ukaze. (na primer, "
"Contol+S za Shrani)"

#: ../tuxpaint-config2.cxx:2246 ../tuxpaint-config2.cxx:2887
msgid "Mouse:"
msgstr "Miška:"

#: ../tuxpaint-config2.cxx:2250
msgid "&Grab Mouse Pointer"
msgstr "&Zagrabi kazalnik miške"

#: ../tuxpaint-config2.cxx:2255
msgid "Prevents the mouse pointer from leaving the Tux Paint window."
msgstr "Prepreči kazalniku miške izhod iz okna Tux slikarja."

#: ../tuxpaint-config2.cxx:2260
msgid "Mouse &Wheel Support"
msgstr "Podpora miškinega &koleščka"

#: ../tuxpaint-config2.cxx:2265
msgid ""
"Allows a mouse wheel to be used to scroll through items. (e.g., brushes, "
"stamps)"
msgstr ""
"Dovoli uporabo miškinega koleščka pri brskanju med predmeti. (na primer "
"čopiči, znamke)"

#: ../tuxpaint-config2.cxx:2270
msgid "No &Button Distinction"
msgstr "Nobenega razlikovanja &gumbov"

#: ../tuxpaint-config2.cxx:2274
msgid "Allows middle and right mouse buttons to be used for clicking, too."
msgstr "Dovoli uporabo srednjega in desnega miškinega gumba za klikanje. "

#. TAB: SIMPLIFCIATION
#. FIXME: From here on, not using 'boxx/boxy' trick, used above -bjk 2011.04.15
#: ../tuxpaint-config2.cxx:2287
msgid "Simplification"
msgstr "Poenostavitev"

#: ../tuxpaint-config2.cxx:2295
msgid "Interface Simplification:"
msgstr "Poenostavitev vmesnika:"

#: ../tuxpaint-config2.cxx:2299
msgid "Disable Shape &Rotation"
msgstr "Onemogoči &vrtenje oblik"

#: ../tuxpaint-config2.cxx:2304
msgid "Shape tool's rotation step is disabled. Useful for younger children."
msgstr ""
"Korak vrtenja v oblikovnih orodjih je onemogočen. Uporabno za mlajše otroke."

#: ../tuxpaint-config2.cxx:2309
msgid "Simple Stamp &Outlines"
msgstr "Preprosti &orisi znamk"

#: ../tuxpaint-config2.cxx:2314
msgid ""
"Draw a rectangle around the cursor when placing stamps, rather than a "
"detailed outline. (For slow computers and thin clients.)"
msgstr ""
"Pri polaganju znamk nariši pravokotnik okoli kazalca namesto podrobnega "
"orisa. (Za počasne računalnike in lahke odjemalce.)"

#: ../tuxpaint-config2.cxx:2319
msgid "Show &Uppercase Text Only"
msgstr "Pokaži le besedilo z &velikimi črkami"

#: ../tuxpaint-config2.cxx:2324
msgid ""
"Cause all text in Tux Paint (button labels, dialogs, etc.) to appear in "
"UPPERCASE rather than Mixed Case."
msgstr ""
"Spremeni vse besedilo v Tux slikarju (oznake gumbov, pogovorna okna, itd.) v "
"VELIKE ČRKE namesto mešanih črk."

#: ../tuxpaint-config2.cxx:2333
msgid "Initial Stamp Size:"
msgstr "Začetna velikost znamke:"

#: ../tuxpaint-config2.cxx:2350
msgid "Control Simplification:"
msgstr "Nadzor poenostavitev:"

#: ../tuxpaint-config2.cxx:2354
msgid "Disable '&Quit' Button and [Escape] key"
msgstr "Onemogoči gumb '&Končaj' in ubežno tipko"

#: ../tuxpaint-config2.cxx:2359
msgid ""
"Clicking the window's close (X) button in the title bar, or pressing "
"[Alt]+[F4] or [Shift]+[Ctrl]+[Escape] will still quit Tux Paint."
msgstr ""
"Klik na gumb za zaprtje (X) v naslovni vrstici, ali pritisk [Alt]+[F4] ali "
"[Shift]+[Ctrl]+[Escape] bo še vedno zaprl Tux slikarja."

#: ../tuxpaint-config2.cxx:2364
msgid "Disable '&Stamps' Tool"
msgstr "Onemogoči orodje '&znamke'"

#: ../tuxpaint-config2.cxx:2369
msgid "Do not load stamps at startup, thus disabling the Stamps tool."
msgstr "Ob zagonu ne naloži znamk, torej onemogoči orodje znamk."

#: ../tuxpaint-config2.cxx:2374
msgid "Disable Stamp &Controls"
msgstr "Onemogoči &nadzor znamk"

#: ../tuxpaint-config2.cxx:2379
msgid ""
"Simplify the 'Stamps' tool by removing the stamp control buttons (Shrink, "
"Grow, Mirror and Flip)."
msgstr ""
"Poenostavi orodje 'Znamke' z odstranitvijo nadzornih gumbov znamke "
"(pomanjšaj, povečaj in zrcali vodoravno in navpično)."

#: ../tuxpaint-config2.cxx:2384
msgid "Disable &Magic Controls"
msgstr "Onemogoči &Čarovniška orodja"

#: ../tuxpaint-config2.cxx:2389
msgid ""
"Simplify the 'Magic' tools by removing the buttons to switch between paint "
"and fullscreen modes."
msgstr ""
"Poenostavi 'Čarovniška' orodja z odstranitvijo gumbov za preklop med "
"slikarskim in celozaslonskim načinom."

#: ../tuxpaint-config2.cxx:2394
#, fuzzy
#| msgid "Disable Stamp &Controls"
msgid "Disable Shape Controls"
msgstr "Onemogoči &nadzor znamk"

#: ../tuxpaint-config2.cxx:2398
#, fuzzy
#| msgid ""
#| "Simplify the 'Magic' tools by removing the buttons to switch between "
#| "paint and fullscreen modes."
msgid ""
"Simplify the 'Shapes' tool by removing the buttons to switch between center- "
"and corner-based drawing."
msgstr ""
"Poenostavi 'Čarovniška' orodja z odstranitvijo gumbov za preklop med "
"slikarskim in celozaslonskim načinom."

#: ../tuxpaint-config2.cxx:2403
msgid "Disable '&Label' Tool"
msgstr "Onemogoči orodje '%Oznaka'"

#: ../tuxpaint-config2.cxx:2408
msgid "Disable the 'Label' text-entry tool (leaving only the 'Text' tool)."
msgstr "Onemogoči orodje 'Oznaka' (ostane le orodje  'Besedilo')."

#. TAB: LANGUAGES
#: ../tuxpaint-config2.cxx:2419
msgid "Languages"
msgstr "Jeziki"

#: ../tuxpaint-config2.cxx:2425
msgid "Language:"
msgstr "Jezik:"

#: ../tuxpaint-config2.cxx:2429
msgid "Language : "
msgstr "Jezik:"

#: ../tuxpaint-config2.cxx:2444
msgid "Run Tux Paint in a particular language (overriding system's settings)."
msgstr "Zažene Tux slikarja v določenem jeziku (povozi sistemske nastavitve)."

#: ../tuxpaint-config2.cxx:2449
msgid "&Mirror Stamps"
msgstr "&Zrcali znamke"

#: ../tuxpaint-config2.cxx:2454
msgid ""
"Automatically mirror-image all mirror-able stamps. Useful for users who "
"prefer things appearing right-to-left."
msgstr ""
"Samodejno zrcali vse znamke, ki jih je mogoče zrcaliti. Uporabno za "
"uporabnike, ki jim je ljubša razporeditev od desne proti levi."

#: ../tuxpaint-config2.cxx:2463
msgid "Fonts:"
msgstr "Pisave:"

#: ../tuxpaint-config2.cxx:2467
msgid "Load System &Fonts"
msgstr "Naloži &sistemske pisave "

#: ../tuxpaint-config2.cxx:2472
msgid ""
"Attempt to load more fonts, found elsewhere on your computer. (Note: may "
"cause instability!)"
msgstr ""
"Poskusi naložiti več pisav, najdenih drugje na računalniku. (Opomba: Lahko "
"povzroči nestabilnost!)"

#: ../tuxpaint-config2.cxx:2477
msgid "Load All &Locale Fonts"
msgstr "Naloži &sistemske pisave "

#: ../tuxpaint-config2.cxx:2482
msgid ""
"Load all locale-specific fonts installed in Tux Paint, regardless of the "
"locale Tux Paint is being run under."
msgstr ""
"Naloži vse pisave, ki so značilne za jezikovne nastavitve v programu Tux "
"Paint, neglede na jezikovni set, ki je sistemsko v  uporabi."

#. TAB: PRINTING
#: ../tuxpaint-config2.cxx:2493
msgid "Printing"
msgstr "Tiskanje"

#: ../tuxpaint-config2.cxx:2499
msgid "Print Permissions:"
msgstr "Dovoljenja tiskanja:"

#: ../tuxpaint-config2.cxx:2503
msgid "Allow &Printing"
msgstr "Dovoli &tiskanje"

#: ../tuxpaint-config2.cxx:2508
msgid "Let users print from within Tux Paint."
msgstr "Uporabnikom dovoli tiskanje iz Tux slikarja."

#: ../tuxpaint-config2.cxx:2513
msgid "Print Delay : "
msgstr "Zamik tiskanja:"

#: ../tuxpaint-config2.cxx:2519
msgid "seconds"
msgstr "sekund"

#: ../tuxpaint-config2.cxx:2523
msgid ""
"Restrict printing to once every N seconds. (Enter '0' to allow unrestricted "
"printing.)"
msgstr ""
"Omeji tiskanje na enkrat na vsakih N sekund. (Vnesite '0' za dovoljenje "
"neomejenega tiskanja.)"

#: ../tuxpaint-config2.cxx:2531
msgid "Show Printer Dialog:"
msgstr "Pokaži pogovorno okno tiskanja:"

#: ../tuxpaint-config2.cxx:2536
msgid "Only when [Alt] &modifier key is held"
msgstr "Le kadar je pritisnjena tipka [Alt]"

#: ../tuxpaint-config2.cxx:2543
msgid "Always &show printer dialog"
msgstr "Vedno &prikaži pogovorno okno tiskanja"

#: ../tuxpaint-config2.cxx:2550
msgid "&Never show printer dialog"
msgstr "&Nikoli ne prikaži pogovornega okna tiskanja"

#: ../tuxpaint-config2.cxx:2557
msgid "(Even when [Alt] is held.)"
msgstr "(Tudi ko je pritisnjen [Alt].)"

#: ../tuxpaint-config2.cxx:2571
msgid "Save printer configuration"
msgstr "Shrani nastavitve tiskalnika"

#: ../tuxpaint-config2.cxx:2580
msgid "Print Commands:"
msgstr "Ukazi tiskanja:"

#: ../tuxpaint-config2.cxx:2584
msgid "Use &Alternative Print Command"
msgstr "Uporabi &dodaten ukaz za tiskanje"

#: ../tuxpaint-config2.cxx:2589
msgid ""
"Override Tux Paint's default setting for print command ('lpr') with another. "
"(Advanced! Unix/Linux only!)"
msgstr ""
"Prepiši Tux slikarjevo privzeto nastavitev za ukaz tiskanje ('lpr') z "
"drugim. (Napredno! Le Unix/Linux!)"

#: ../tuxpaint-config2.cxx:2599
msgid ""
"Enter the command for printing. It must accept a PostScript format on its "
"standard input (STDIN)."
msgstr ""
"Vnos ukaza za tiskanje. Mora sprejeti obliko PostScript na standardnem vhodu "
"(STDIN)."

#: ../tuxpaint-config2.cxx:2610
msgid "Use &Alternative Print Dialog"
msgstr "Uporabi &dodatno pogovorno okno tiskanja"

#: ../tuxpaint-config2.cxx:2615
msgid ""
"Override Tux Paint's default setting for print dialog ('kprinter') with "
"another. (Advanced! Unix/Linux only!)"
msgstr ""
"Prepiši Tux slikarjevo privzeto nastavitev za pogovorno okno tiskanja "
"('kprinter') z drugo. (Napredno! Le Unix/Linux!)"

#: ../tuxpaint-config2.cxx:2625
msgid ""
"Enter the print dialog command. It must accept a PostScript format on its "
"standard input (STDIN)."
msgstr ""
"Vnos ukaza pogovornega okna tiskanja. Mora sprejeti obliko PostScript na "
"standardnem vhodu (STDIN)."

#: ../tuxpaint-config2.cxx:2632
msgid "Paper Size : "
msgstr "Velikost papirja:"

#. TAB: SAVING
#: ../tuxpaint-config2.cxx:2653
msgid "Saving"
msgstr "Shranjevanje"

#: ../tuxpaint-config2.cxx:2660
msgid "Save Over Earlier Work:"
msgstr "Shrani čez starejše delo:"

#: ../tuxpaint-config2.cxx:2664
msgid "&Ask Before Overwriting"
msgstr "&Vprašaj pred prepisovanjem"

#: ../tuxpaint-config2.cxx:2669
msgid ""
"When re-saving an image, ask whether to overwrite the earlier version, or "
"make a new file."
msgstr ""
"Pri ponovnem shranjevanju slike vpraša, ali naj prepiše prejšnjo različico "
"ali ustvari novo datoteko."

#: ../tuxpaint-config2.cxx:2674
msgid "Always &Overwrite Older Version"
msgstr "Vedno &prepiši starejšo različico"

#: ../tuxpaint-config2.cxx:2679
msgid ""
"When re-saving an image, always overwrite the earlier version. (Warning: "
"Potential for lost work!)"
msgstr ""
"Pri ponovnem shranjevanju slik vedno prepiši prejšnjo različico (Opozorilo: "
"Obstaja možnost izgubljenega dela!)"

#: ../tuxpaint-config2.cxx:2684
msgid "Always Save &New Picture"
msgstr "Vedno shrani &novo sliko"

#: ../tuxpaint-config2.cxx:2689
msgid ""
"When re-saving an image, always make a new file. (Warning: Potential for "
"lots of files!)"
msgstr ""
"Pri ponovnem shranjevanju slik vedno ustvari novo datoteko. (Opozorilo: "
"Obstaja možnost velikega števila datotek!)"

#: ../tuxpaint-config2.cxx:2696
msgid "Starting Out:"
msgstr ""

#: ../tuxpaint-config2.cxx:2701
msgid "Start with &Blank Canvas"
msgstr "Začni s &praznim platnom"

#. Place solid color backgrounds at the end of the "New" dialog, promoting pre-drawn Starters and Templates to the top
#: ../tuxpaint-config2.cxx:2708
msgid "Colors last in 'New' dialog"
msgstr ""

#: ../tuxpaint-config2.cxx:2717
#, fuzzy
#| msgid "Save Directory:"
msgid "Save and Export Directories:"
msgstr "Mapa za shranjevanje:"

#. Save directory
#: ../tuxpaint-config2.cxx:2722
msgid "Use &Alternative Save Directory"
msgstr "Uporabi &dodatno shranjevalno mapo"

#: ../tuxpaint-config2.cxx:2727
msgid ""
"Do not save pictures in the standard directory, use the following location:"
msgstr "Ne shrani slik v standardni mapi, uporabi sledeče mesto: "

#: ../tuxpaint-config2.cxx:2732
msgid "Alternative Save Directory:"
msgstr "Dodatna shranjevalna mapa:"

#: ../tuxpaint-config2.cxx:2737 ../tuxpaint-config2.cxx:2757
#: ../tuxpaint-config2.cxx:2836 ../tuxpaint-config2.cxx:2864
msgid "Browse..."
msgstr "Prebrskaj ..."

#. Export directory
#: ../tuxpaint-config2.cxx:2743
#, fuzzy
#| msgid "Use &Alternative Data Directory"
msgid "Use Alternative Export Directory"
msgstr "Uporabi &dodatno podatkovno mapo "

#: ../tuxpaint-config2.cxx:2747
#, fuzzy
#| msgid ""
#| "Do not save pictures in the standard directory, use the following "
#| "location:"
msgid ""
"Do not export pictures and animated GIFs in the standard directory, use the "
"following location:"
msgstr "Ne shrani slik v standardni mapi, uporabi sledeče mesto: "

#: ../tuxpaint-config2.cxx:2752
#, fuzzy
#| msgid "Alternative Data Directory:"
msgid "Alternative Export Directory:"
msgstr "Dodatna podatkovna mapa:"

#: ../tuxpaint-config2.cxx:2763
msgid "More Saving Options:"
msgstr "Več možnosti shranjevanja:"

#: ../tuxpaint-config2.cxx:2768
msgid "Disable '&Save' Button"
msgstr "Onemogoči gumb '&shrani'"

#: ../tuxpaint-config2.cxx:2774
msgid "&Auto-save on Quit"
msgstr "&Samodejno shrani ob končanju"

#: ../tuxpaint-config2.cxx:2779
msgid "Don't ask to save current picture when quitting; just save."
msgstr "Ne vpraša, ali naj shrani sliko ob končanju; samo shrani."

#. TAB: DATA
#: ../tuxpaint-config2.cxx:2791
msgid "Data"
msgstr "Podatki"

#. FIXME: Looks awful:
#: ../tuxpaint-config2.cxx:2799
msgid "Lockfile:"
msgstr "Datoteka ključa:"

#: ../tuxpaint-config2.cxx:2803
msgid "&Don't use lockfile"
msgstr "&Ne uporabi datoteke ključa"

#: ../tuxpaint-config2.cxx:2807
msgid ""
"Do not check for a lockfile. Allow Tux Paint to be launched multiple times. "
"(May be necessary in a networked environment.)"
msgstr ""
"Ne preveri za datoteko ključa. Dovoli večkraten zagon Tux slikarja. (Morda "
"je možnost zaželena v omreženem okolju.)"

#: ../tuxpaint-config2.cxx:2817
msgid "Data Directory:"
msgstr "Podatkovna mapa:"

#: ../tuxpaint-config2.cxx:2821
msgid "Use &Alternative Data Directory"
msgstr "Uporabi &dodatno podatkovno mapo "

#: ../tuxpaint-config2.cxx:2826
msgid ""
"Do not load brushes, stamps, etc. from the standard directory, use the "
"following location:"
msgstr ""
"Ne naloži čopičev, znamk, itn. iz standardne mape; uporabi sledeče mesto:"

#: ../tuxpaint-config2.cxx:2831
msgid "Alternative Data Directory:"
msgstr "Dodatna podatkovna mapa:"

#: ../tuxpaint-config2.cxx:2845 ../tuxpaint-config2.cxx:2859
msgid "Color Palette File:"
msgstr "Datoteka palete barv:"

#: ../tuxpaint-config2.cxx:2849
msgid "Use &Alternative Color Palette"
msgstr "Uporabi &dodatno barvno paleto"

#: ../tuxpaint-config2.cxx:2854
msgid ""
"Don't use default color palette, use colors defined in the following file:"
msgstr ""
"Ne uporabi privzete barvne palete, uporabi barve določene v sledeči datoteki:"

#: ../tuxpaint-config2.cxx:2881
msgid "Accessibility"
msgstr "Dostopnost"

#: ../tuxpaint-config2.cxx:2891
msgid "Sticky mouse clicks"
msgstr "Lepljivi kliki miške"

#. Fl_Check_Button* o
#: ../tuxpaint-config2.cxx:2895
msgid ""
"Useful for users who have difficulty clicking and dragging. When enabled, "
"click and release to start painting, move to paint, and click and release "
"again to stop. It can also be combined with joystick- and keyboard-based "
"pointer controls."
msgstr ""
"Možnost se izkaže pri uporabnikih, ki imajo težave s klikanjem in vlečenjem. "
"Izbrana možnost omogoči, da s klikom in sprostitvijo miške začnemo risati, s "
"premikanjem vlečemo sled in s ponovnim klikom in sprostitvijo tipke miške "
"risanje zaustavimo. Možnost je mogoče uporabljati tudi z igralno palico in "
"drugimi napravami."

#: ../tuxpaint-config2.cxx:2906
msgid "Keyboard controls the mouse pointer"
msgstr "Tipkovnica upravlja s kazalniko miške"

#. Fl_Check_Button* o
#: ../tuxpaint-config2.cxx:2910
msgid ""
"When enabled, the arrow keys or numbers can be used to move the mouse "
"pointer. Number 5, Space, Insert or F5 can be used to click. (When in \"Text"
"\" or \"Label\" tools, the numbers and space cannot be used.) Also, F4 "
"cycles the pointer between the \"Tools\" box, \"Colors\" box and drawing "
"canvas, F8 and F7 move up/down inside the \"Tools\" box, and F11 and F12 "
"move left/right inside the \"Tools\" and \"Colors\" boxes."
msgstr ""
"Izbrana možnost omogoča premikanje kazalke miške s smernimi tipkami in "
"številčnico. Številka 5, preslednica, tipka Vstavi ali F5 so določene kot "
"klik. (Pri orodjih \"Besedilo\" in \"Oznaka\" so številke in preslednica "
"onemogočene) Tipka F4 preklaplja izbiro kazalke med \"Orodji\", \"Barvami\" "
"in risalno površino, tipki F8 in F7 premikata predmete gor in dol, tipki F11 "
"in F12 pa levo in desno med različnimi okni preklopa."

#: ../tuxpaint-config2.cxx:2918
msgid "Onscreen keyboard:"
msgstr "Zaslonska tipkovnica:"

#: ../tuxpaint-config2.cxx:2922
msgid "Show a keyboard on the screen"
msgstr "Pokaži tipkovnico na zaslonu"

#. Fl_Check_Button* o
#: ../tuxpaint-config2.cxx:2926
msgid ""
"Display a keyboard on the screen when the \"Text\" and \"Label\" tools are "
"enabled, so you can 'type' with the mouse pointer."
msgstr ""
"Prikaži tipkovnico na zaslonu, kadar sta omogočeni orodji \"Besedilo\" in "
"\"Oznaka\", da bo mogoče pisati s kazalko miške. "

#. Fl_Box* o
#: ../tuxpaint-config2.cxx:2931
msgid "Layout"
msgstr " Razporeditev"

#. Fl_Choice* o
#: ../tuxpaint-config2.cxx:2943
msgid "How are keys organized in the keyboard"
msgstr "Kako so tipke na tipkovnici razporejene"

#. Fl_Box* o
#: ../tuxpaint-config2.cxx:2948
msgid "Disable layout changes"
msgstr "Onemogoči spremembe razporeditve"

#. Fl_Check_Button* o
#: ../tuxpaint-config2.cxx:2953
msgid "Disable the buttons that allow changing the keyboard layout."
msgstr ""
"Onemogoči gumbe, ki dovolijo spreminjanje razporeditve tipk tipkovnice."

#. Fl_Group* o
#. TAB: JOYSTICK
#: ../tuxpaint-config2.cxx:2965
msgid "Joystick"
msgstr "Igralna palica"

#. o->box(FL_PLASTIC_UP_BOX);
#: ../tuxpaint-config2.cxx:2976
msgid "Main device:"
msgstr "Glavna naprava:"

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:2986
msgid "Device number to use. (SDL starts numbering at 0)"
msgstr "Številka naprave za uporabo (SDL štetje se začne z 0)."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:2997
msgid ""
"Joystick slowness. Increase this value for people with slow reactions. "
"(0-500; default value is 15)"
msgstr ""
"Hitrost igralne palice. Za igralce, ki niso hitro odzivni, je vrednost "
"priporočljivo povečati (0-500; privzeto je določena vrednost 15)."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3009
msgid ""
"SDL events under threshold will be discarded, useful to discard accidental "
"unwanted movements. (0-32766; default value is 3200)"
msgstr ""
"Dogodki SDL, ki so pod določenim pragom, bodo zavrženi. Nastavitev je "
"priročna, kadar želimo onemogočiti nenadzorovane premike (0-32766; privzeto "
"je določena vrednost 3200)."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3019
msgid "Limit speed when moving. (1-7; default value is 7)"
msgstr "Omeji hitrost premikanja (1-7; privzeto je določena vrednost 7)."

#. Fl_Group* o
#: ../tuxpaint-config2.cxx:3026
msgid "Hat:"
msgstr "Enakomerno gibanje:"

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3035
msgid "Hat slowness. (0-500; default value is 15)"
msgstr "Hitrost enakomernega gibanja (0-500; privzeta vrednost je 15)"

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3047
msgid ""
"Hat timeout, the number of milliseconds to wait before starting to move "
"continuously. (0-3000; default value is 1000)"
msgstr ""
"Časovni zamik, po katerem se gibanje opredeli kot enakomerno (0-3000; "
"privzeto določena vrednost je 1000)"

#. Fl_Group* o
#: ../tuxpaint-config2.cxx:3058
msgid "Buttons to disable:"
msgstr "Onemogoči gumbe:"

#. Fl_Input* o
#: ../tuxpaint-config2.cxx:3074
msgid ""
"If for any reason one or more buttons of the joystick are posing problems, "
"you can disable them here using a comma separated list of button numbers. (e."
"g. 2,3,5)"
msgstr ""
"Če zaradi kateregakoli razloga gumbi igralne palice povzročajo težave, jih "
"je dobro onemogočiti z vpisom na seznam, ločenim z vejico (na primer 2, 3, 4)"

#: ../tuxpaint-config2.cxx:3083
msgid "Button shortcuts:"
msgstr "Bližnjice gumbov:"

#: ../tuxpaint-config2.cxx:3087
msgid ""
"Here you can configure shortcuts for the different buttons of the joystick. "
"(Beware to not assign a shortcut to the button used to draw.)"
msgstr ""
"Na tem mestu je mogoče nastaviti bližnjice za različne gumbe igralne palice "
"(ne nastavite bližnjice za gumb premikanja)."

#. Fl_Box* o
#: ../tuxpaint-config2.cxx:3092
msgid "Button number for the Escape key."
msgstr "Številka gumba za ubežno tipko."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3100
msgid "Button number for the Brush tool."
msgstr "Številka gumba za orodje čopiča"

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3108
msgid "Button number for the Stamps tool."
msgstr "Številka gumba za orodje žigov."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3116
msgid "Button number for the Lines tool."
msgstr "Številka gumba za orodje črt."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3124
msgid "Button number for the Shapes tool."
msgstr "Številka gumba za orodje oblik."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3132
msgid "Button number for the Text tool."
msgstr "Številka gumba za orodje za besedilo."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3140
msgid "Button number for the Label tool."
msgstr "Številka gumba za orodje naziva."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3148
msgid "Button number for the Magic tool."
msgstr "Številka gumba za orodje čarovniško palico."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3156
msgid "Button number for Undo."
msgstr "Številka gumba za razveljavitev."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3164
msgid "Button number for Redo."
msgstr "Številka gumba za ponovno uveljavitev."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3172
msgid "Button number for the Eraser tool."
msgstr "Številka gumba za brisalko."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3180
msgid "Button number for the New tool."
msgstr "Številka gumba za novo orodje."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3188
msgid "Button number for the Open tool."
msgstr "Številka gumba za orodje za odpiranje."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3196
msgid "Button number for saving."
msgstr "Številka gumba za shranjevanje."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3204
msgid "Button number for Page Setup."
msgstr "Številka gumba za nastavitev strani."

#. Fl_Spinner* o
#: ../tuxpaint-config2.cxx:3212
msgid "Button number for printing."
msgstr "Številka gumba za tiskanje."

#: ../tuxpaint-config2.cxx:3234
msgid "Settings for : "
msgstr "Nastavitve za:"

#: ../tuxpaint-config2.cxx:3238
msgid "Current User"
msgstr "Trenutni uporabnik"

#: ../tuxpaint-config2.cxx:3239
msgid "All Users"
msgstr "Vsi uporabniki"

#: ../tuxpaint-config2.cxx:3245
msgid "Use \"All Users\" Settings"
msgstr "Uporabi nastavitve \"Vsi uporabniki\""

#: ../tuxpaint-config2.cxx:3249
msgid "Apply"
msgstr "Uporabi"

#: ../tuxpaint-config2.cxx:3257
msgid "Reset"
msgstr "Ponastavi"

#: ../tuxpaint-config2.cxx:3264
msgid "Defaults"
msgstr "Privzeto"

#: ../tuxpaint-config2.cxx:3271
msgid "Quit"
msgstr "Končaj"

#. vim:ts=8:sts=3
#: ../tuxpaint-config.desktop.in.h:1
msgid "Tux Paint Config."
msgstr "Tux Paint nastavitve"

#: ../tuxpaint-config.desktop.in.h:2
msgid "Configure Tux Paint"
msgstr "Nastavitve Tux Paint"

#~ msgid ""
#~ "Don't allow pictures to be saved. Tux Paint acts as temporary 'scratch "
#~ "paper.'"
#~ msgstr ""
#~ "Ne dovoli shranitve slik. Tux slikar se obnaša kot začasen 'papir za "
#~ "zapiske.'"

#~ msgid "Start Blank:"
#~ msgstr "Začni prazno:"
