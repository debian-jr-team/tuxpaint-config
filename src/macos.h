#ifndef __MACOS_H__
#define __MACOS_H__

#if __cplusplus
extern "C" {
#endif


#define MACOS_BASE_CONFIG_T   0x00  /* tuxpaint.cfg                                            */
#define MACOS_TEMP_CONFIG_T   0x01  /* /tmp/tuxpaint.cfg.XXXXXX                                */
#define MACOS_USER_CONFIG_T   0x02  /* $HOME/Library/Application Support/TuxPaint/tuxpaint.cfg */
#define MACOS_GLOBAL_CONFIG_T 0x03  /* /Library/Application Support/TuxPaint/tuxpaint.cfg      */

const char* macos_filename(int config_t);
const char* macos_dirname(const char* filename);
const char* macos_error();

int macos_mkdir(const char* dirname);
int macos_unlink(const char* filename);
int macos_sudo_cp(const char* source, const char* target);


#if __cplusplus
}  // extern "C"
#endif

#endif /* __MACOS_H__ */
